from python:alpine

# need pipenv bin folder on path
env PATH="/root/.local/bin:${PATH}"

# we need pipenv
run pip install --user pipenv

# add application
copy app /app
workdir /app

# install dependncies
run pipenv install

# lets go
cmd pipenv run python manage.py runserver 0:8000
