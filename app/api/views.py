from django.shortcuts import render
from django.http import JsonResponse


# Create your views here.
def index(req):
    res = {
        'users': [
            {
                'name': 'boll, grodan boll',
                'color': 'green'
            },
            {
                'name': 'james bond',
                'color': 'tan'
            },
        ]
    }
    return JsonResponse(res)
