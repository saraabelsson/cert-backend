# cert-backend

python without knowledge of python

## Todo

- [ ] Create a Todo

## setup

Pre-requirements: Python3

**_Create virtual environment_**

```
cd app
python -m venv <name-of-env>
```

add <name-of-env> to .gitignore and .dockerignore

**_Windows_**

```
<name-of-env>\Scripts\activate.bat
```

**_Linux_**

```
. <name-of-env>\bin\activate
```

if using fish

```
. <name-of-env>\bin\activate.fish
```

**_Install_**

```
pip install pipenv
pipenv install
```

**_Start server_**

```
python manage.py runserver
```

### IDE: PyCharm

File -> Settings | Project: cert-backend | Project Interpreter
gear up right -> add | existing environment -> ... <find/path/to/python>

**_linux_** <name-of-env>/bin/python

**_windows_** <name-of-env>\Scripts\python.exe

Ok -> Ok

## Goals

- learn some python via django
- likely just crud?
